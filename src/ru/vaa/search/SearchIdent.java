package ru.vaa.search;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchIdent {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("\\b[A-Za-z_]\\w*\\b");
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Work\\Identificatory\\src\\ru\\vaa\\search\\test.txt"))){
            String str;
            while((str = bufferedReader.readLine()) != null) {
                Matcher m = p.matcher(str);
                while (m.find()) {
                    System.out.println(m.group());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}